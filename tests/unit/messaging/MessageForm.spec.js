import Vuex from 'vuex';
import {createLocalVue, mount} from '@vue/test-utils';
import MessageForm from "@/components/MessageForm";

const localVue = createLocalVue();
localVue.use(Vuex);

describe('MessageForm', () => {
    describe('when not logged in', () => {
        const loginModule = {
            state: {
                loggedin: false
            },
            getters: {
                loggedin: (state) => {
                    return state.loggedin;
                }
            },
        };

        const store = new Vuex.Store ({
            modules: {
                login: loginModule
            }
        })

        const wrapper = mount(MessageForm, {localVue, store});

        it('displays login', () => {
            expect(wrapper.find('.login__heading').exists()).toBe(true);
        });
    });

    describe('when logged in', () => {
        const loginModule = {
            state: {
                loggedin: true
            },
            getters: {
                loggedin: (state) => {
                    return state.loggedin;
                }
            },
        };

        const store = new Vuex.Store ({
            modules: {
                login: loginModule
            }
        })

        const wrapper = mount(MessageForm, {localVue, store});

        it('does not display login', () => {
            expect(wrapper.find('.login__heading').exists()).toBe(false);
        });
    });
});
